program LazWatchDog;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Interfaces,SysUtils, Dialogs,// this includes the LCL widgetset
  Forms, MainFormU, ProcessListFormU, AsStringUtils, ProcessUtils,
  VersionSupport, AboutFormU, CpuMemActionFormU { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TProcessListForm, ProcessListForm);
 Application.CreateForm(TAboutForm, AboutForm);
 Application.CreateForm(TCPUMemActionForm, CPUMemActionForm);
  Application.Run;
end.


