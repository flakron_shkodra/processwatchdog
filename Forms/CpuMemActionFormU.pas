unit CpuMemActionFormU;

{$mode objfpc}{$H+}

interface

uses
 Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
 Buttons, StdCtrls;

type

 { TCPUMemActionForm }

 TCPUMemActionForm = class(TForm)
  btnOk: TBitBtn;
  btnCancel: TBitBtn;
  grpActions: TRadioGroup;
  lblInfo: TLabel;
  memProcessInfos: TMemo;
 private
  { private declarations }
 public
  { public declarations }
 end;

var
 CPUMemActionForm: TCPUMemActionForm;

implementation

{$R *.lfm}

end.

