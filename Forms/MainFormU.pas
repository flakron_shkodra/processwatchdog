unit MainFormU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, Menus, Spin, ExtCtrls, PopupNotifier, EditBtn, ComCtrls,
  ProcessUtils, AsStringUtils, CpuMemActionFormU, ServiceManager, process,
  registry, IniFiles, Windows, eventlog, strutils, JwaAclApi, JwaAccCtrl;

type

  { TMainForm }

  TMainForm = class(TForm)
   btnAdd: TSpeedButton;
   btnAddToWhiteList: TSpeedButton;
   btnAddServiceToBlackList: TSpeedButton;
   btnListProcesses: TSpeedButton;
    btnOk: TBitBtn;
    btnClose: TBitBtn;
    chkCpuMemEnabled: TCheckBox;
    chkServiceStopper: TCheckBox;
    chkProcessStarterEnabled: TCheckBox;
    chkCpuAutokill: TCheckBox;
    chkAutostart: TCheckBox;
    chkMemAutokill: TCheckBox;
    chkShowNotification: TCheckBox;
    grpCpuMemCondition: TGroupBox;
    grpCPUMem: TGroupBox;
    grpWatchProcess: TGroupBox;
    ImageList1: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    lblQuickSearchService: TLabel;
    lblBlackList: TLabel;
    lblServices: TLabel;
    lblCpumemPercent: TLabel;
    lblCpuUsage: TLabel;
    lblKB: TLabel;
    lblMemUsage: TLabel;
    lblWhiteList: TLabel;
    lblStartApplicationPath: TLabel;
    lstServices: TListBox;
    lstServiceBlackList: TListBox;
    lstWhiteList: TListBox;
    lstWatchProcesses: TListBox;
    mitDeleteWL: TMenuItem;
    mitDeleteBL1: TMenuItem;
    notifier: TPopupNotifier;
    PageControl1: TPageControl;
    popWhiteList: TPopupMenu;
    popServiceBlackList: TPopupMenu;
    seCpuUsage: TSpinEdit;
    seMemUsage: TSpinEdit;
    SvcMgr: TServiceManager;
    tabProcessStarter: TTabSheet;
    grpOptions: TGroupBox;
    lblWatchInterval: TLabel;
    lbLSecond: TLabel;
    mitExitApp: TMenuItem;
    mitSep1: TMenuItem;
    mitApp: TMenuItem;
    mitAbout: TMenuItem;
    mitExit: TMenuItem;
    mitHelp: TMenuItem;
    mitFile: TMenuItem;
    mmMainMenu: TMainMenu;
    mitDelete: TMenuItem;
    popProcess: TPopupMenu;
    popApplication: TPopupMenu;
    ProcessStarter: TProcess;
    sdWatchInterval: TSpinEdit;
    tabProcessCPUMEM: TTabSheet;
    tabServices: TTabSheet;
    tmrCheckNotifier: TTimer;
    tmrWatchTimer: TTimer;
    TrayIcon1: TTrayIcon;
    txtProcessName: TEdit;
    txtSearchService: TEdit;
    txtStartParams: TEdit;
    txtStartProcessPath: TFileNameEdit;
    procedure btnAddClick(Sender: TObject);
    procedure btnAddServiceToBlackListClick(Sender: TObject);
    procedure btnAddToWhiteListClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnListProcessesClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure chkAutostartChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblServicesClick(Sender: TObject);
    procedure lstWatchProcessesClick(Sender: TObject);
    procedure mitAboutClick(Sender: TObject);
    procedure mitAppClick(Sender: TObject);
    procedure mitDeleteBL1Click(Sender: TObject);
    procedure mitDeleteClick(Sender: TObject);
    procedure mitDeleteWLClick(Sender: TObject);
    procedure mitExitAppClick(Sender: TObject);
    procedure mitExitClick(Sender: TObject);
    procedure sdWatchIntervalChange(Sender: TObject);
    procedure SvcMgrAfterConnect(Sender: TObject);
    procedure tabServicesShow(Sender: TObject);
    procedure tmrCheckNotifierTimer(Sender: TObject);
    procedure tmrCpuMemActionTimer(Sender: TObject);
    procedure tmrWatchTimerTimer(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure TrayIcon1DblClick(Sender: TObject);
    procedure OnCPUReportProcess(ProcInfo:TProcessInfoEx);
    procedure OnMemReportProcess(ProcInfo:TProcessInfoEx);
    procedure txtSearchServiceChange(Sender: TObject);
  private
    FLogger:TEventLog;
    FProcessListFile: string;
    FWhiteListFile:string;
    FServiceBlackListFile:string;
    FProcLister: TProcessLister;
    FProcMonitor:TProcessMonitor;
    FCurrentCpuMemReportProcess:TProcessInfoEx;

    procedure QuickSearchService;
    procedure ShowCpuMemAction;
    procedure AddProcessToWatch(ProcessName: string; StartProcessPath:string; StartProcessParams:string);
    procedure AddToWhiteList(ProcessName:string);
    procedure AddToServiceBlackList(ServiceName:string);
    procedure CloseApplication;
    procedure ReadAppSettings;
    procedure SaveAppSettings;
    procedure StartWatching;
    procedure WriteLog(Msg:string);
    function ItemExists(Item:string):Boolean;
    procedure FillServices;

  public
    { public declarations }
  end;



var
  AppName:string = 'LazWatchdog';
  AppVersion:string;

var
  MainForm: TMainForm;

implementation

uses ProcessListFormU, AboutFormU, VersionSupport;

{$R *.lfm}

{ TMainForm }




procedure TMainForm.FormCreate(Sender: TObject);
begin
  AppVersion:=VersionSupport.GetFileVersion;

  PageControl1.ActivePageIndex:=0;

  FLogger := TEventLog.Create(Self);
  FLogger.LogType:=ltFile;
  FLogger.DefaultEventType:=etInfo;
  FLogger.Identification:=AppName;
  FLogger.Active:=True;

  FProcLister := TProcessLister.Create;
  FProcessListFile := Application.ExeName + '.pl';
  FWhiteListFile:=Application.ExeName+'.wl';
  FServiceBlackListFile := Application.ExeName+'.sbl';

  if FileExists(FProcessListFile) then
  begin
    lstWatchProcesses.Items.LoadFromFile(FProcessListFile);
  end;

  if FileExists(FWhiteListFile) then
  begin
    lstWhiteList.Items.LoadFromFile(FWhiteListFile);
  end;


  if FileExists(FServiceBlackListFile) then
  begin
    lstServiceBlackList.Items.LoadFromFile(FServiceBlackListFile);
  end;

  ReadAppSettings;

  TrayIcon1.Icon:=Application.Icon;
  TrayIcon1.Visible := True;

  with notifier do
  begin
    Icon.Assign(Application.Icon);
    Title := 'LazWatchdog';
    vNotifierForm.Width := 250;
    vNotifierForm.Height := 60;
    vNotifierForm.AutoHide := True;
    vNotifierForm.HideInterval := 2;
    vNotifierForm.AlphaBlend := True;
    vNotifierForm.AlphaBlendValue := 200;
  end;

  Caption := AppName + ' ' + AppVersion;

end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FProcLister.Free;
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  if WindowState = wsMinimized then
  begin
    Hide;
  end;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  if (ParamStr(1) = 's') and (Self.Tag = 0) then
  begin
    StartWatching;
  end;
end;

procedure TMainForm.lblServicesClick(Sender: TObject);
begin
 FillServices;
end;

procedure TMainForm.lstWatchProcessesClick(Sender: TObject);
var
  AppItem:TArrayOfString;
  PathParam:TArrayOfString;
begin
  if lstWatchProcesses.ItemIndex<0 then exit;
 AppItem := TAsStringUtils.SplitString(lstWatchProcesses.Items[lstWatchProcesses.ItemIndex], '=');
 if Length(AppItem)=2 then
 begin
   txtProcessName.Text:=AppItem[0];
   PathParam:=TAsStringUtils.SplitString(AppItem[1],'|');
   if Length(PathParam)=2 then
   begin
    txtStartProcessPath.Text:=PathParam[0];
    txtStartParams.Text:=PathParam[1];
   end;
 end;
end;

procedure TMainForm.mitAboutClick(Sender: TObject);
begin
 AboutForm.ShowModal;
end;

procedure TMainForm.mitAppClick(Sender: TObject);
begin
  Self.Tag := 1;
  Self.Show;
  tmrWatchTimer.Enabled := False;

  if FProcMonitor<>nil then
  FProcMonitor.Terminate;
end;

procedure TMainForm.mitDeleteBL1Click(Sender: TObject);
begin
 if lstServiceBlackList.ItemIndex>-1 then
 begin
  lstServiceBlackList.Items.Delete(lstServiceBlackList.ItemIndex);
  lstServiceBlackList.Items.SaveToFile(FServiceBlackListFile);
 end;
end;

procedure TMainForm.mitDeleteClick(Sender: TObject);
begin
  if lstWatchProcesses.ItemIndex > -1 then
  begin
    lstWatchProcesses.Items.Delete(lstWatchProcesses.ItemIndex);
    lstWatchProcesses.Items.SaveToFile(FProcessListFile);
  end;
end;

procedure TMainForm.mitDeleteWLClick(Sender: TObject);
begin
 if lstWhiteList.ItemIndex > -1 then
  begin
    lstWhiteList.Items.Delete(lstWhiteList.ItemIndex);
    lstWhiteList.Items.SaveToFile(FWhiteListFile);
  end;
end;

procedure TMainForm.mitExitAppClick(Sender: TObject);
begin
  CloseApplication;
end;

procedure TMainForm.mitExitClick(Sender: TObject);
begin
  CloseApplication;
end;


procedure TMainForm.sdWatchIntervalChange(Sender: TObject);
begin
  tmrWatchTimer.Enabled := False;
  tmrWatchTimer.Interval := sdWatchInterval.Value * 1000;
end;

procedure TMainForm.SvcMgrAfterConnect(Sender: TObject);
begin
 ShowMessage('connected');
end;



procedure TMainForm.tabServicesShow(Sender: TObject);
begin
 FillServices;
end;

procedure TMainForm.tmrCheckNotifierTimer(Sender: TObject);
begin
  tmrCheckNotifier.Enabled := False;
  if notifier.Visible then
    notifier.Hide;
end;

procedure TMainForm.tmrCpuMemActionTimer(Sender: TObject);
begin


end;

procedure TMainForm.tmrWatchTimerTimer(Sender: TObject);
var
  I: integer;
  AppItem: TArrayOfString;
  StartApp:TArrayOfString;
  StartParams:string;
  Msg:string;
  s:TSERVICESTATUS;
  se:TServiceEntry;
begin
  tmrWatchTimer.Enabled := False;

  if chkAutostart.Checked then
  for I := 0 to lstWatchProcesses.Count - 1 do
  begin
    AppItem := TAsStringUtils.SplitString(lstWatchProcesses.Items[i], '=');
    if (Length(AppItem) = 2) then
    begin


      if not FProcLister.IsRunning(AppItem[0]) then
      begin
        StartApp:=TAsStringUtils.SplitString(AppItem[1],'|');
        StartParams:=EmptyStr;
        if Length(StartApp)=2 then
        StartParams:=StartApp[1];

        if FProcLister.StartProcess(StartApp[0],StartParams) then
        begin
          if chkShowNotification.Checked then
          begin
            Msg :='Process started: [' + AppItem[0]+ ']';
            WriteLog(Msg);
            notifier.Text := Msg;
            notifier.ShowAtPos(Screen.Width, Screen.Height);
            tmrCheckNotifier.Enabled := True;
          end;
        end;
      end;

    end;
  end;

  if chkServiceStopper.Checked then
  begin
   SvcMgr.Refresh;
   for I:=0 to lstServiceBlackList.Count-1 do
   begin
     se := SvcMgr.Services.FindService(lstServiceBlackList.Items[I]);
     if se.CurrentState = SERVICE_RUNNING then
     begin
       SvcMgr.StopService(lstServiceBlackList.Items[I],True);
       if chkShowNotification.Checked then
       begin
         Msg :='Stopping service: [' + lstServiceBlackList.Items[I]+ ']';
         WriteLog(Msg);
         notifier.Text := Msg;
         notifier.ShowAtPos(Screen.Width, Screen.Height);
         tmrCheckNotifier.Enabled := True;
       end;
     end;
   end;

  end;

  tmrWatchTimer.Enabled := True;
end;

procedure TMainForm.TrayIcon1Click(Sender: TObject);
begin
  TrayIcon1.BalloonTitle := 'Info';
  TrayIcon1.BalloonHint := 'LazWatchDog (RightClick for menu)';
  TrayIcon1.BalloonFlags := bfInfo;
  TrayIcon1.ShowBalloonHint;
end;

procedure TMainForm.TrayIcon1DblClick(Sender: TObject);
begin
  TrayIcon1Click(Sender);
end;

procedure TMainForm.OnCPUReportProcess(ProcInfo: TProcessInfoEx);
begin
  FCurrentCpuMemReportProcess:=ProcInfo;
  FProcMonitor.Synchronize(FProcMonitor,@ShowCpuMemAction);
end;

procedure TMainForm.OnMemReportProcess(ProcInfo: TProcessInfoEx);
begin
  FCurrentCpuMemReportProcess:=ProcInfo;
  FProcMonitor.Synchronize(FProcMonitor, @ShowCpuMemAction);
end;

procedure TMainForm.txtSearchServiceChange(Sender: TObject);
begin
 QuickSearchService;
end;

procedure TMainForm.ShowCpuMemAction;

begin


  if CPUMemActionForm.Visible then exit;
  if not Assigned(FCurrentCpuMemReportProcess) then exit;


   with CPUMemActionForm do
   begin
     lblInfo.Caption:='A process exceeded cpu/mem values set for monitoring';
     memProcessInfos.Clear;
     memProcessInfos.Lines.Add('Process ID: '+IntToStr(FCurrentCpuMemReportProcess.PID));
     memProcessInfos.Lines.Add('Process Name: '+FCurrentCpuMemReportProcess.Name);
     memProcessInfos.Lines.Add('CPU Usage: '+IntToStr(trunc(FCurrentCpuMemReportProcess.CpuUsage)) );
     memProcessInfos.Lines.Add('Mem Usage: '+FloatToStr(FCurrentCpuMemReportProcess.MemUsage div 1024) +' KB');
     if ShowModal=mrOK then
     begin
       case grpActions.ItemIndex of
       0:AddToWhiteList(FCurrentCpuMemReportProcess.Name);
       1:FProcLister.KillProcess(FCurrentCpuMemReportProcess);
       end;
     end;
   end;

end;

procedure TMainForm.QuickSearchService;
var
 I: Integer;
begin
 if Trim(txtSearchService.Text)=EmptyStr then
 begin
  lstServices.ItemIndex:=0;
   Exit;
 end;

 for I:=0 to lstServices.Count -1 do
 begin
   if AnsiContainsText(lstServices.Items[I], txtSearchService.Text) then
   begin
     lstServices.ItemIndex:=I;
     Break;
   end;
 end;
end;

procedure TMainForm.AddProcessToWatch(ProcessName: string;
 StartProcessPath: string; StartProcessParams: string);
begin
  lstWatchProcesses.Items.Add(ProcessName+'='+StartProcessPath+'|'+StartProcessParams);
  lstWatchProcesses.Items.SaveToFile(FProcessListFile);
  txtProcessName.Clear;
end;

procedure TMainForm.AddToWhiteList(ProcessName: string);
begin
  if lstWhiteList.Items.IndexOf(ProcessName)<0 then
  begin

    lstWhiteList.Items.Add(ProcessName);
    lstWhiteList.Items.SaveToFile(FWhiteListFile);
    if Assigned(FProcMonitor) then
      if FProcMonitor.WhiteList.IndexOf(ProcessName)<0 then
        FProcMonitor.WhiteList.Add(ProcessName);
  end;
end;

procedure TMainForm.AddToServiceBlackList(ServiceName: string);
begin
  if lstServiceBlackList.Items.IndexOf(ServiceName)<0 then
  begin
    lstServiceBlackList.Items.Add(ServiceName);
    lstServiceBlackList.Items.SaveToFile(FServiceBlackListFile);
  end;
end;

procedure TMainForm.CloseApplication;
begin
  SaveAppSettings;
  Application.Terminate;
end;

procedure TMainForm.ReadAppSettings;
begin
  with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
  begin
    chkAutostart.Checked := ReadBool('Settings', 'Autostart', True);
    chkShowNotification.Checked := ReadBool('Settings', 'Notification', True);
    sdWatchInterval.Value := ReadInteger('Settings', 'WatchInterval', 2);

    seMemUsage.Value:=ReadInteger('Settings', 'MemUsage', 100000);
    seCpuUsage.Value:=ReadInteger('Settings', 'CpuUsage', 25);
    chkCpuAutokill.Checked:= ReadBool('Settings', 'CpuAutokill', false);
    chkMemAutokill.Checked:= ReadBool('Settings', 'MemAutokill', false);

    chkCpuMemEnabled.Checked:=ReadBool('Settings', 'CpuMemMonitorActive', false);
    chkProcessStarterEnabled.Checked:=ReadBool('Settings', 'ProcStarterActive', false);
    chkServiceStopper.Checked:= ReadBool('Settings','ServiceStopperActive',false);

    tmrWatchTimer.Interval:=sdWatchInterval.Value*1000;

    Free;
  end;
end;

procedure TMainForm.SaveAppSettings;
begin
  with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
  begin
    WriteBool('Settings', 'Autostart', chkAutostart.Checked);
    WriteBool('Settings', 'Notification', chkShowNotification.Checked);
    WriteInteger('Settings', 'WatchInterval', sdWatchInterval.Value);

    WriteInteger('Settings', 'MemUsage', seMemUsage.Value);
    WriteInteger('Settings', 'CpuUsage', seCpuUsage.Value);
    WriteBool('Settings', 'CpuAutokill', chkCpuAutokill.Checked);
    WriteBool('Settings', 'MemAutokill', chkMemAutokill.Checked);

    WriteBool('Settings', 'CpuMemMonitorActive', chkCpuMemEnabled.Checked);
    WriteBool('Settings', 'ProcStarterActive', chkProcessStarterEnabled.Checked);

    WriteBool('Settings','ServiceStopperActive',chkServiceStopper.Checked);

    Free;
  end;
end;

procedure TMainForm.StartWatching;
begin
 if lstWatchProcesses.Items.Count > 0 then
  begin
    tmrWatchTimer.Interval:=sdWatchInterval.Value*1000;

    if chkProcessStarterEnabled.Checked or chkServiceStopper.Checked then
     begin
        if (lstWatchProcesses.Count>0) or (lstServiceBlackList.Count>0) then
        tmrWatchTimer.Enabled := True;
     end;

    if chkCpuMemEnabled.Checked then
    begin
      FProcMonitor := TProcessMonitor.Create;
      FProcMonitor.AutoKillOnCpu:=chkCpuAutokill.Checked;
      FProcMonitor.AutoKillOnMem:= chkCpuAutokill.Checked;
      FProcMonitor.OnCpuUsageReport:=@OnCPUReportProcess;
      FProcMonitor.OnMemUsageReprot:=@OnMemReportProcess;
      FProcMonitor.CpuUsageToReport:=seCpuUsage.Value;
      FProcMonitor.MemUsageToReport:=seMemUsage.Value*1024;
      FProcMonitor.CheckInterval:=sdWatchInterval.Value*1000;
      FProcMonitor.WhiteList.Clear;
      FProcMonitor.WhiteList.AddStrings(lstWhiteList.Items);
      FProcMonitor.Start;

    end;

    Hide;
    TrayIcon1.BalloonFlags := bfInfo;
    TrayIcon1.BalloonTitle := AppName + ' ' + AppVersion;
    TrayIcon1.BalloonHint := 'Application running in background' + LineEnding;
    TrayIcon1.ShowBalloonHint;
  end;
end;

procedure TMainForm.WriteLog(Msg: string);
begin
 FLogger.Info(Msg);
end;

function TMainForm.ItemExists(Item: string): Boolean;
var
 I: Integer;
begin
 Result:=false;
 for I:=0 to lstWatchProcesses.Count-1 do
 begin
  if AnsiStartsText(Item, lstWatchProcesses.Items[I]) then
  begin
    Result:=True;
    Break;
  end;
 end;
end;

procedure TMainForm.FillServices;
var
  s:TServiceEntry;
  I: Integer;
begin

 if SvcMgr.Connected then
    SvcMgr.Disconnect;

  SvcMgr.MachineName := 'KSPR1WS041';

  try
    SvcMgr.Connect;

  except
    on e: Exception do
      ShowMessage(e.Message);
  end;

  lstServices.Clear;

   for I:=0 to SvcMgr.Services.Count-1 do
   begin
     s := SvcMgr.Services[I];
     lstServices.Items.Add(s.ServiceName+'| ['+ServiceStateToString(s.CurrentState)+'] - '+s.DisplayName)
   end;


end;

procedure TMainForm.btnAddClick(Sender: TObject);
begin
  if (Trim(txtProcessName.Text) <> EmptyStr) and (FileExists(txtStartProcessPath.Text)) then
  begin
    if not ItemExists(txtProcessName.Text) then
    begin
     AddProcessToWatch(txtProcessName.Text,txtStartProcessPath.Text,txtStartParams.Text);
     txtProcessName.Text:=EmptyStr;
     txtStartProcessPath.Text:=EmptyStr;
     txtStartParams.Text:=EmptyStr;
    end else
    begin
     ShowMessage(txtProcessName.Text+' already exists. Try another process name');
    end;
  end else
  begin
    ShowMessage('Process name should not be empty and Start Process path should be existing applicatoin');
  end;
end;

procedure TMainForm.btnAddServiceToBlackListClick(Sender: TObject);
var
   s:string;
begin
 if lstServices.ItemIndex>-1 then
 begin
  s :=lstServices.Items[lstServices.ItemIndex];
  AddToServiceBlackList(TAsStringUtils.SplitString(s,'|')[0]);
 end;
end;

procedure TMainForm.btnAddToWhiteListClick(Sender: TObject);
begin
 with TOpenDialog.Create(nil) do
 begin
  Filter:='Executables (*.exe)|*.exe';
  DefaultExt:='exe';
  if Execute then
  begin
    AddToWhiteList(ExtractFileName(FileName));
  end;
  Free;
 end;
end;

procedure TMainForm.btnCloseClick(Sender: TObject);
begin

end;

procedure TMainForm.btnListProcessesClick(Sender: TObject);
begin

  with ProcessListForm do
    if ShowModal = mrOk then
    begin
      txtProcessName.Text := ChangeFileExt(lstProcesses.Items[lstProcesses.ItemIndex], '');
      with TProcessLister.Create  do
      begin
        try
          try
            txtStartProcessPath.Text:=GetProcessPath(txtProcessName.Text);
          except
          end;
        finally
          Free;
        end;
      end;
    end;
end;

procedure TMainForm.btnOkClick(Sender: TObject);
begin
  StartWatching;
  SaveAppSettings;
end;


procedure TMainForm.chkAutostartChange(Sender: TObject);
var
  r: TRegistry;
begin
  r := TRegistry.Create;
  try
    r.RootKey := HKEY_LOCAL_MACHINE;

    if r.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', False) then
    begin

      if chkAutostart.Checked then
      begin
        r.WriteString(AppName, Application.ExeName + ' s');
      end
      else
      begin
        r.DeleteValue(AppName);
      end;

    end;

  finally
    r.Free;
  end;
end;

procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  SaveAppSettings;
end;

end.
