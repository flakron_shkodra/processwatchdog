unit AboutFormU;

{$mode objfpc}{$H+}

interface

uses
 Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
 StdCtrls, Buttons;

type

 { TAboutForm }

 TAboutForm = class(TForm)
  Bevel1: TBevel;
  btnOK: TBitBtn;
  Image1: TImage;
  lblAuthor: TLabel;
  lblVersion: TLabel;
  lblAPP: TLabel;
  txtDescription: TMemo;
  procedure FormCreate(Sender: TObject);
 private
  { private declarations }
 public
  { public declarations }
 end;

var
 AboutForm: TAboutForm;

implementation

uses MainFormU;

{$R *.lfm}

{ TAboutForm }

procedure TAboutForm.FormCreate(Sender: TObject);
begin
 lblAPP.Caption:=AppName;
 lblVersion.Caption:=AppVersion;
end;

end.

