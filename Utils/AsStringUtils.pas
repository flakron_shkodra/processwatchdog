unit AsStringUtils;

{$mode objfpc}{$H+}
{$Static ON}


interface

uses
  Classes, SysUtils,db;

type

  { TAsStringUtils }

  TArrayOfString = array of string;

  TAsStringUtils = object
  public
    {Splits string into array of string based on delimiter char given}
    class function SplitString(Text: string; Delimiter: char): TArrayOfString;

  end;

implementation

{ TAsStringUtils }

class function TAsStringUtils.SplitString(Text: string;
  Delimiter: char): TArrayOfString;
var
  lst: TStringList;
  I: integer;
begin
  try
    lst := TStringList.Create;
    lst.StrictDelimiter:=True;
    lst.Delimiter := Delimiter;
    lst.DelimitedText := Text;

    SetLength(Result, lst.Count);

    for I := 0 to lst.Count - 1 do
    begin
      Result[I] := lst[I];
    end;

  finally
    lst.Free;
  end;
end;

end.


