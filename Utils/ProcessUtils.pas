{
  *******************************************************************
  AUTHOR : Flakron Shkodra 2013
  *******************************************************************
}

unit ProcessUtils;

{$mode objfpc}{$H+}

interface

uses
  Classes,SysUtils,windows,JwaTlHelp32,JwaPsApi,process,fgl;

type

  TProcessInfo = class
  public
    PID:Cardinal;
    Name:string;
  end;

  TProcessInfoEx = class
  public
    PID:Cardinal;
    Name:string;
    CpuUsage:Double;
    MemUsage:Cardinal;
  end;

  { TProcessInfoList }

  TProcessInfoList = class (specialize TFPGObjectList<TProcessInfo>)
  public
    function FindProcess(PID:Cardinal):TProcessInfo;
    function FindFirstProcess(Name:string):TProcessInfo;
  end;


  { TProcessLister }

  TProcessLister = class
  private
   FLastError: string;
    proc:TProcess;
    procedure SetLastError(AValue: string);
    function ForceKill(pi:TProcessInfoEx):Boolean;overload;
  public
    constructor Create;
    function GetCpuUsage(PID:Cardinal):Double;
    function GetProcessPath(ProcessName:string):string;
    function GetProcessID(ProcessName:string):Cardinal;
    function GetProcessHandle(PID:cardinal):THandle;
    function GetProcessMemory(PID:cardinal):Cardinal;
    function GetRunningProcesses:TStringList;
    function GetRunningProcessesInfo:TProcessInfoList;
    function KillProcess(pi: TProcessInfoEx): Boolean; overload;
    function StartProcess(ExePath:string; Params:string):Boolean;
    function IsRunning(ProcessName:string):Boolean;
    function GetProcessInfoEx(PID:Cardinal):TProcessInfoEx;
    property LastError:string read FLastError;
    destructor Destroy;override;
  end;


  TProcessMonitorEvent = procedure(ProcInfo:TProcessInfoEx) of object;
  { TProcessMonitor }

  TProcessMonitor = class(TThread)
  private
   FAutoKillOnMem: Boolean;
   FAutoKillOnCpu: Boolean;
   FCheckInterval: Integer;
    FCpuUsageToReport: double;
    FMemUsageToReport: double;
    FOnCpuUsageReport: TProcessMonitorEvent;
    FOnMemUsageReprot: TProcessMonitorEvent;
    FProcessLister: TProcessLister;
    FWhiteList: TStringList;
    procedure SetAutoKill(AValue: Boolean);
    procedure SetAutoKillOnCpu(AValue: Boolean);
    procedure SetCheckInterval(AValue: Integer);
    procedure SetCpuUsageToReport(AValue: double);
    procedure SetMemUsageToReport(AValue: double);
    procedure SetOnCpuUsageReport(AValue: TProcessMonitorEvent);
    procedure SetOnMemUsageReprot(AValue: TProcessMonitorEvent);
    procedure SetWhiteList(AValue: TStringList);
  protected
    procedure Execute; override;
  public
    constructor Create;
    destructor Destroy; override;
    property CpuUsageToReport: double read FCpuUsageToReport write SetCpuUsageToReport;
    property MemUsageToReport: double read FMemUsageToReport write SetMemUsageToReport;
    property OnCpuUsageReport: TProcessMonitorEvent      read FOnCpuUsageReport write SetOnCpuUsageReport;
    property OnMemUsageReprot:TProcessMonitorEvent read FOnMemUsageReprot write SetOnMemUsageReprot;
    property AutoKillOnMem:Boolean read FAutoKillOnMem write SetAutoKill;
    property AutoKillOnCpu:Boolean read FAutoKillOnCpu write SetAutoKillOnCpu;
    property WhiteList:TStringList read FWhiteList write SetWhiteList;
    property CheckInterval:Integer read FCheckInterval write SetCheckInterval;
  end;

implementation

{ TProcessInfoList }

function TProcessInfoList.FindProcess(PID: Cardinal): TProcessInfo;
var
 I: Integer;
begin
 Result:=nil;
 for I:=0 to Count-1 do
 begin
   if Items[I].PID=PID then
   begin
    Result:=Items[I];
    break;
   end;
 end;
end;

function TProcessInfoList.FindFirstProcess(Name: string): TProcessInfo;
var
 I: Integer;
begin
 Result:=nil;
 for I:=0 to Count-1 do
 begin
   if Items[I].Name=Name then
   begin
    Result:=Items[I];
    break;
   end;
 end;
end;

{ TProcessMonitor }

procedure TProcessMonitor.SetCpuUsageToReport(AValue: double);
begin
FCpuUsageToReport:=AValue;
end;

procedure TProcessMonitor.SetAutoKill(AValue: Boolean);
begin
 if FAutoKillOnMem=AValue then Exit;
 FAutoKillOnMem:=AValue;
end;

procedure TProcessMonitor.SetAutoKillOnCpu(AValue: Boolean);
begin
 if FAutoKillOnCpu=AValue then Exit;
 FAutoKillOnCpu:=AValue;
end;

procedure TProcessMonitor.SetCheckInterval(AValue: Integer);
begin
 if FCheckInterval=AValue then Exit;
 FCheckInterval:=AValue;
end;

procedure TProcessMonitor.SetMemUsageToReport(AValue: double);
begin
 if FMemUsageToReport=AValue then Exit;
 FMemUsageToReport:=AValue;
end;

procedure TProcessMonitor.SetOnCpuUsageReport(AValue: TProcessMonitorEvent);
begin
  FOnCpuUsageReport:=AValue;
end;

procedure TProcessMonitor.SetOnMemUsageReprot(AValue: TProcessMonitorEvent);
begin
 if FOnMemUsageReprot=AValue then Exit;
 FOnMemUsageReprot:=AValue;
end;

procedure TProcessMonitor.SetWhiteList(AValue: TStringList);
begin
 if FWhiteList=AValue then Exit;
 FWhiteList:=AValue;
end;

procedure TProcessMonitor.Execute;
var
  cpu: double;
  pid : cardinal;
  I: integer;
  lst: TProcessInfoList;
  pi:TProcessInfoEx;
begin
  while not Terminated do
  begin
    lst := FProcessLister.GetRunningProcessesInfo;
    try

      for I := 0 to lst.Count - 1 do
      begin

        if FWhiteList.IndexOf(lst[I].Name)>-1 then
        Continue;

        if lst[I].PID = GetCurrentProcessId then
        Continue;

        pi := FProcessLister.GetProcessInfoEx(lst[i].PID);

        if pi=nil then
        Continue;

        try

          if pi.CpuUsage >= FCpuUsageToReport then
          begin
            if FAutoKillOnCpu then
            begin
              FProcessLister.KillProcess(pi);
            end else
            if Assigned(FOnCpuUsageReport) then
            begin
              FOnCpuUsageReport(pi);
            end;
          end;//END CPU CHECK

          if pi.MemUsage>FMemUsageToReport then
          begin
            if FAutoKillOnMem then
            begin
              FProcessLister.KillProcess(pi);
            end else
            if Assigned(FOnMemUsageReprot) then
            begin
              FOnMemUsageReprot(pi);
            end;
          end; // END MEM CHECK

        finally
          pi.Free;
        end;

      end;//END FOR


    finally
      lst.Free;
    end;
    Sleep(FCheckInterval);
  end;
end;

constructor TProcessMonitor.Create;
begin
  FProcessLister := TProcessLister.Create;
  inherited Create(True);
  FreeOnTerminate:=True;
  FWhiteList := TStringList.Create;
end;

destructor TProcessMonitor.Destroy;
begin
 FProcessLister.Free;
 FWhiteList.Free;
 inherited Destroy;
end;

{ TProcessLister }

procedure TProcessLister.SetLastError(AValue: string);
begin
 if FLastError=AValue then Exit;
 FLastError:=AValue;
end;

function TProcessLister.ForceKill(pi: TProcessInfoEx): Boolean;
var
  sysdir:array [0..MaxPathLen] of char;
begin
    GetSystemDirectory(@sysdir,Length(sysdir));
    proc.ApplicationName:=sysdir+'\taskkill.exe';
    proc.CommandLine:='taskkill.exe /F /PID '+IntToStr(pi.PID)+' /T';
    proc.ShowWindow:=swoHIDE;
    try
      proc.Execute;
      Result:=True;
    except on e:Exception do
      begin
        FLastError:=e.Message;
        Result := False;
      end;
    end;
end;

function TProcessLister.GetCpuUsage(PID: Cardinal): Double;
const
  cWaitTime = 250;
var
  h: cardinal;
  mCreationTime, mExitTime, mKernelTime, mUserTime: _FILETIME;
  TotalTime1, TotalTime2: int64;
begin
  h := OpenProcess(MAXIMUM_ALLOWED, False, PID);
  GetProcessTimes(h, mCreationTime, mExitTime, mKernelTime, mUserTime);
  TotalTime1 := int64(mKernelTime.dwLowDateTime or (mKernelTime.dwHighDateTime shr 32)) +
    int64(mUserTime.dwLowDateTime or (mUserTime.dwHighDateTime shr 32));


  Sleep(cWaitTime);

  GetProcessTimes(h, mCreationTime, mExitTime, mKernelTime, mUserTime);
  TotalTime2 := int64(mKernelTime.dwLowDateTime or (mKernelTime.dwHighDateTime shr 32)) +
    int64(mUserTime.dwLowDateTime or (mUserTime.dwHighDateTime shr 32));

  Result := ((TotalTime2 - TotalTime1) / cWaitTime) / 100;
  CloseHandle(h);
end;

constructor TProcessLister.Create;
begin
  inherited Create;
  proc := TProcess.Create(nil);
  proc.ApplicationName:='tskill.exe';
end;

function TProcessLister.GetProcessPath(ProcessName: string): string;
var
  vSnapshot: THandle;
  vModuleEntry: TModuleEntry32;
begin
 Result:='';
  vSnapshot := CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetProcessID(ProcessName));
  if (vSnapShot=0) or (vSnapshot=INVALID_HANDLE_VALUE) then Exit;
  try
    vModuleEntry.dwSize := SizeOf(TModuleEntry32);
    if not Module32First(vSnapshot, vModuleEntry) then
      RaiseLastWin32Error;

    Result := vModuleEntry.szExePath;
  finally
    CloseHandle(vSnapshot);
  end;
end;

function TProcessLister.GetProcessID(ProcessName: string): Cardinal;
var
  pa: TProcessEntry32;
  RetVal: THandle;
  sList : TStringList;
  _procName:string;
  GivenProcName:string;
begin
  Result := 0;
  GivenProcName:=lowercase(ProcessName);

  if ExtractFileExt(GivenProcName)='' then
  begin
    GivenProcName:= ChangeFileExt(GivenProcName,'.exe');
  end;

  RetVal := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pa.dwSize := sizeof(pa);

  if Process32First(RetVal, pa) then
  begin
    _procName:= lowercase(pa.szExeFile);
    if (_procName=GivenProcName) then
    begin
      Result := pa.th32ProcessID;
    end else
    while Process32Next(RetVal, pa)  do
    begin
      _procName := lowercase(pa.szExeFile);
      if _procName = GivenProcName then
      begin
        Result := pa.th32ProcessID;
        Break;
      end;
    end;
  end;

end;

function TProcessLister.GetProcessHandle(PID: cardinal): THandle;
var
  pa: TProcessEntry32;
  RetVal: THandle;
  sList : TStringList;
begin
  Result := 0;

  RetVal := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pa.dwSize := sizeof(pa);

  if Process32First(RetVal, pa) then
  begin
    if (pid = pa.th32ProcessID) then
    begin
      Result := pa.th32ProcessID;
    end else
    while Process32Next(RetVal, pa)  do
    begin
      if (pid = pa.th32ProcessID) then
      begin
        Result := OpenProcess(PROCESS_ALL_ACCESS, FALSE, pa.th32ProcessID);;
        Break;
      end;
    end;
  end;

end;

function TProcessLister.GetProcessMemory(PID: cardinal): Cardinal;
var

  tmpHandle: HWND;
  pmc: PPROCESS_MEMORY_COUNTERS;
  pmcSize: Cardinal;
begin
  pmcSize := SizeOf(PROCESS_MEMORY_COUNTERS);
  GetMem(pmc, pmcSize);
  pmc^.cb := pmcSize;
  tmpHandle := OpenProcess(MAXIMUM_ALLOWED, TRUE, PID);

  if (GetProcessMemoryInfo(tmpHandle, (pmc^), pmcSize)) then
    pmcSize := pmc^.WorkingSetSize
  else
  begin
    pmcSize := 0;
  end;

  FreeMem(pmc);

  Result := pmcSize;

end;

function TProcessLister.GetRunningProcesses: TStringList;
var
  pa: TProcessEntry32;
  RetVal: THandle;
  sList : TStringList;
begin
  RetVal := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pa.dwSize := sizeof(pa);
  sList := TStringList.Create;
  if Process32First(RetVal, pa) then
    sList.Add(pa.szExeFile);

    while Process32Next(RetVal, pa) do
    begin
      sList.Add(pa.szExeFile);
    end;

  Result := sList;
end;

function TProcessLister.GetRunningProcessesInfo: TProcessInfoList;
var
  pa: TProcessEntry32;
  RetVal: THandle;

  pi:TProcessInfo;
  lstPI:TProcessInfoList;
begin
  lstPI := TProcessInfoList.Create;

  RetVal := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  pa.dwSize := sizeof(pa);

  if Process32First(RetVal, pa) then
  begin
    pi := TProcessInfo.Create;
    pi.PID:= pa.th32ProcessID;
    pi.Name:= pa.szExeFile;
    lstPI.Add(pi);
  end;

  while Process32Next(RetVal, pa) do
  begin
    pi := TProcessInfo.Create;
    pi.PID:= pa.th32ProcessID;
    pi.Name:= pa.szExeFile;
    lstPI.Add(pi);
  end;
  Result := lstPI;
end;
function TProcessLister.KillProcess(pi: TProcessInfoEx): Boolean;
var
  h,rv:Cardinal;
  sysdir:array [0..MaxPathLen] of char;
begin
  if h<>0 then
  begin
   Result := TerminateProcess(
            OpenProcess(PROCESS_TERMINATE, BOOL(0),h),
            0
          );

   //burte force
   if not Result then
   begin
    Result := ForceKill(pi);
   end;

  end;
end;

function TProcessLister.StartProcess(ExePath: string; Params: string): Boolean;
begin
 proc.ApplicationName:=ExePath;
 proc.CommandLine:=ExtractFileName(ExePath)+' '+Params;
 proc.ShowWindow:=swoShowNormal;
 proc.CurrentDirectory:=ExtractFilePath(ExePath);
 try
  proc.Execute;
  Sleep(2000);
  Result := proc.Running;
 except on e:Exception do
  begin
    FLastError:=e.Message;
    Result:=False;
  end;
 end;
end;

function TProcessLister.IsRunning(ProcessName: string): Boolean;
var
  lst:TStringList;
  I: Integer;
begin
 Result:=False;
 try
  lst:=GetRunningProcesses;
  lst.Text:=LowerCase(lst.Text);
  Result := lst.IndexOf(ChangeFileExt(lowercase(ProcessName),'.exe'))>-1;
 finally
   if Assigned(lst) then lst.Free;
 end;

end;

function TProcessLister.GetProcessInfoEx(PID: Cardinal): TProcessInfoEx;
var
  tmpList:TProcessInfoList;
  tmp:TProcessInfo;
  pi:TProcessInfoEx;
begin
 Result:=nil;
 try
    tmpList := GetRunningProcessesInfo;
    tmp := tmpList.FindProcess(PID);
    if tmp<>nil then
    begin
     pi := TProcessInfoEx.Create;
     pi.PID:=tmp.PID;
     pi.Name:= tmp.Name;
     pi.CpuUsage:= GetCpuUsage(PID);
     pi.MemUsage:=GetProcessMemory(PID);
     Result := pi;
    end;
 finally
   tmpList.Free;
 end;
end;

destructor TProcessLister.Destroy;
begin
  proc.Free;
  inherited Destroy;
end;


end.

